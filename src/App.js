import React, { useState, useEffect } from 'react'; // React & Hooks
import { Credentials } from './Credentials/Credentials'; //Là où se trouve le clientId & clientSecret pour pouvoir utiliser l'API spotify
import axios from 'axios'; // Pour intéragir avec SpotifyAPI
import { Grid, Button } from '@material-ui/core'; // Pour l'affichage
import { makeStyles } from '@material-ui/core/styles'; // Pour le style


// import Assets from './Assests/Helvetica Bold';
const App = () => {
  //J'ai utilisé des hooks que je trouve plus facile à lire par rapport au State

  // Pour avoir le clientId et ClientSecret afin d'utiliser les api de spotify
  const spotify = Credentials();  

  // Pour stocker le token afin de l'utiliser dans l'app
  const [token, setToken] = useState('');

  // Pour chercher la playlist selon l'id
  const [idPlaylist, setIdPlaylist] = useState('');

  // Pour stocker la playlist après l'appel de l'api (setPlaylist)
  const [playlist, setPlaylist] = useState('');

  // Variable pour afficher la partie Résultat [false => vide, true => le résultat affiché]
  const [detail, setDetail] = useState(null);

  //Appel de l'api pour avoir un token lorsque la page est chargée.
  useEffect(() => {
    //GET - Pour récupréer un token
    axios('https://accounts.spotify.com/api/token', {
      headers: {
        'Content-Type' : 'application/x-www-form-urlencoded',
        'Authorization' : 'Basic ' + btoa(spotify.ClientId + ':' + spotify.ClientSecret)      
      },
      data: 'grant_type=client_credentials',
      method: 'POST'
    })
    .then(tokenResponse => {  
      //Stockage du token    
      setToken(tokenResponse.data.access_token);
    });

  }, [spotify.ClientId, spotify.ClientSecret]); 

  // Faire la recherche, appelé axios en GET pour récupérer la playlist demandée, la stocké dans la variable playlist
  const buttonClicked = e => {
    
    e.preventDefault();//Pour ne pas rechargé la page
    if(idPlaylist === ""){
      alert("Veuillez entrer l'id de la playlist que vous voulez trouvé");
    }else{
      axios(`https://api.spotify.com/v1/playlists/${idPlaylist}`, {
        method: 'GET',
        headers: {
          'Authorization' : 'Bearer ' + token
        }
      })
      .then(playlistResponse => {
        console.log(playlistResponse.data.images[0].url);
        setPlaylist(playlistResponse.data);
        setDetail(true);
      });
    }
  }

  // Un peu de style ca fait toujours bien 
  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      overflow: 'hidden',
      padding: theme.spacing(0, 3),
      backgroundImage: `linear-gradient(to bottom, rgb(192,192,192), rgb(0,0,0))`,
      color:'white',
      textAlign:'center'
    },
    imgPlaylist:{
      textAlign:'center',
      borderImage: 'linear-gradient(to right, #0083c5 0%, #0083c5 33%, #ec4a26 66%, #ec4a26 100%)',
      borderImageSlice: 1
    },
    classGrid: {
      textAlign: 'center'
    },
    playlistName: {
      fontSize: '2.3em'
    },
    trackName: {
      fontSize: '1.2em',
      textAlign:'left',
      fontFamily:"Helvetica",
      fontWeight:'bold'
    },
    artistName: {
      fontSize: 'x-small',
      textAlign:'left',
      color:'rgb(192,192,192)'
    },
    searchBtn: {
      textAlign: 'center',
      marginLeft:'10em'
    },
    clearBtn: {
      textAlign:'right'
    }
  }));
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <form onSubmit={buttonClicked}>
          <input id="idplaylist" type="text" placeholder="Entrer l'id de la playlist" onChange={
            (e) => {
              setIdPlaylist(e.target.value);
            }
          }/> 
          <div className="col-sm-6 row form-group px-0">
            <button type='submit' className="btn btn-success" id={classes.searchBtn}>
              Search
            </button>
            <Button className="clearBtn" onClick={() => {
              setDetail(false);
              setIdPlaylist('');
            }}>
              Clear
            </Button>
          </div>
          <div className="row">
            { detail ? 
              <div>
                <Grid container spacing={1} direction="column" alignItems="center" className={classes.classGrid}>
                  <Grid item xs={12} textAlign='center'>
                    <div className={classes.imgPlaylist}>
                      <img className="imagePlaylist" alt="Playlist Image" src={playlist.images[0].url} height={playlist.images[0].height} width={playlist.images[0].width}/>
                    </div>
                  </Grid>
                  <Grid item xs={12}>
                    <p className={classes.playlistName}>[{playlist.name}]</p>
                  </Grid>
                  <Grid container item xs={12} direction="column">
                    {playlist.tracks.items.map((track , index) => {
                      return(
                        <Grid key={index} container item className={classes.classGrid} direction="column">
                          <Grid item className={classes.trackName}>{track.track.name}</Grid>
                          <Grid item className={classes.artistName}>{track.track.artists[0].name}</Grid>
                        </Grid>
                      )
                    })}
                  </Grid>
                </Grid>
              </div>
            : null
            }
          </div>        
      </form>
    </div>
  );
}

export default App;